import argparse

parser = argparse.ArgumentParser(description="Convert EnsemblID to gene name")
parser.add_argument("InFile")
parser.add_argument("OutFile")
args = parser.parse_args()


conversion_dict = {}
with open("Gene_Name_Table_Ensembl100", 'r') as f:
    for line in f:
        line = line.rstrip().split("\t")
        conversion_dict[line[0]] = line[1]

if __name__ == "__main__":

    with open(args.InFile, 'r') as in_file, open(args.OutFile,'w') as out_file:
        for line in in_file:
            line = line[:line.find(".")]

            out_file.write(conversion_dict.get(line, "Not Found")+"\n")
